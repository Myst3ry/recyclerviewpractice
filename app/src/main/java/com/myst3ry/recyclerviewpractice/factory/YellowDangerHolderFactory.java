package com.myst3ry.recyclerviewpractice.factory;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myst3ry.recyclerviewpractice.DangerAdapter;
import com.myst3ry.recyclerviewpractice.R;

public final class YellowDangerHolderFactory implements DangerHolderFactory {

    @Override
    public DangerAdapter.YellowDangerViewHolder createDangerHolder(@NonNull ViewGroup parent, @NonNull LayoutInflater inflater) {
        final View itemView = inflater.inflate(R.layout.item_yellow_level, parent, false);
        return new DangerAdapter.YellowDangerViewHolder(itemView);
    }
}
