package com.myst3ry.recyclerviewpractice.factory;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

public interface DangerHolderFactory {

    RecyclerView.ViewHolder createDangerHolder(@NonNull ViewGroup parent, @NonNull LayoutInflater inflater);
}
