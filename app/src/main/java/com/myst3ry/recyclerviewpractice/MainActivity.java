package com.myst3ry.recyclerviewpractice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class MainActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private DangerAdapter mAdapter;
    private DangersReceiver mReceiver;
    private IntentFilter mIntentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        startService();
        initReceiver();
        initUI();
    }

    private void startService() {
        startService(DangerGenService.newIntent(this));
    }

    private void initReceiver() {
        mReceiver = new DangersReceiver();
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(IntentConstants.ACTION_SEND_INITIAL_DANGERS);
        mIntentFilter.addAction(IntentConstants.ACTION_SEND_DANGER);
    }

    private void initUI() {
        mAdapter = new DangerAdapter();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(LinearSpacingItemDecoration.newBuilder()
                .orientation(LinearLayoutManager.VERTICAL)
                .spacing(getResources().getDimensionPixelSize(R.dimen.margin_half))
                .includeEdge(true)
                .build());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mReceiver, mIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    private final class DangersReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case IntentConstants.ACTION_SEND_INITIAL_DANGERS:
                        mAdapter.setInitialDangers(intent.<DangerItem>getParcelableArrayListExtra(IntentConstants.EXTRA_DANGERS_SET));
                        break;
                    case IntentConstants.ACTION_SEND_DANGER:
                        mAdapter.addDangerItem(intent.<DangerItem>getParcelableExtra(IntentConstants.EXTRA_DANGER));
                        mRecyclerView.scrollToPosition(0);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
