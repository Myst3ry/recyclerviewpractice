package com.myst3ry.recyclerviewpractice;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.myst3ry.recyclerviewpractice.pojo.GreenDanger;
import com.myst3ry.recyclerviewpractice.pojo.RedDanger;
import com.myst3ry.recyclerviewpractice.pojo.YellowDanger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public final class DangerGenService extends Service {

    private static final int RANDOM_INIT_DANGERS_MAX = 7;
    private static final int DANGERS_DELAY = 5000;
    private static final int DANGERS_PERIOD = 2500;

    private String[] mDescriptions;

    @Override
    public void onCreate() {
        super.onCreate();
        mDescriptions = getApplicationContext().getResources().getStringArray(R.array.dangers_descriptions);
        prepareInitialDangersList();
        startDangersBroadcast();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static Intent newIntent(final Context context) {
        return new Intent(context, DangerGenService.class);
    }

    private void prepareInitialDangersList() {
        final int dangersCount = getRandomInt(RANDOM_INIT_DANGERS_MAX);
        final List<DangerItem> dangers = new ArrayList<>(dangersCount);

        for (int i = 0; i < dangersCount; i++) {
            dangers.add(getRandomDanger());
        }

        sendInitialDangerList(dangers);
    }

    private DangerItem getRandomDanger() {
        final String dangerDesc = mDescriptions[getRandomInt(mDescriptions.length)];
        final int dangerType = getRandomInt(DangerType.values().length);
        DangerItem dangerItem = null;
        switch (dangerType) {
            case 0:
                dangerItem = new GreenDanger(dangerDesc);
                break;
            case 1:
                dangerItem = new YellowDanger(dangerDesc);
                break;
            case 2:
                dangerItem = new RedDanger(dangerDesc);
                break;
            default:
                break;
        }

        return dangerItem;
    }

    private int getRandomInt(final int range) {
        return new Random().nextInt(range);
    }

    private void sendInitialDangerList(final List<DangerItem> dangers) {
        final Intent initIntent = new Intent();
        initIntent.setAction(IntentConstants.ACTION_SEND_INITIAL_DANGERS);
        initIntent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        initIntent.putExtra(IntentConstants.EXTRA_DANGERS_SET, new ArrayList<>(dangers));
        sendBroadcast(initIntent);
    }

    private void startDangersBroadcast() {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                final DangerItem dangerItem = getRandomDanger();
                sendDanger(dangerItem);
            }
        }, DANGERS_DELAY, DANGERS_PERIOD);
    }

    private void sendDanger(final DangerItem dangerItem) {
        final Intent dangerIntent = new Intent();
        dangerIntent.setAction(IntentConstants.ACTION_SEND_DANGER);
        dangerIntent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        dangerIntent.putExtra(IntentConstants.EXTRA_DANGER, dangerItem);
        sendBroadcast(dangerIntent);
    }
}
