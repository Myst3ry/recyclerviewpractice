package com.myst3ry.recyclerviewpractice;

public final class IntentConstants {

    public static final String ACTION_SEND_INITIAL_DANGERS = BuildConfig.APPLICATION_ID + "action.SEND_INITIAL_DANGERS";
    public static final String ACTION_SEND_DANGER = BuildConfig.APPLICATION_ID + "action.SEND_DANGER";

    public static final String EXTRA_DANGERS_SET = BuildConfig.APPLICATION_ID + "extra.DANGERS_SET";
    public static final String EXTRA_DANGER = BuildConfig.APPLICATION_ID + "extra.DANGER";

    private IntentConstants() {}
}
