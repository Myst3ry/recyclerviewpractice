package com.myst3ry.recyclerviewpractice.binder;

import android.support.v7.widget.RecyclerView;

import com.myst3ry.recyclerviewpractice.DangerAdapter;
import com.myst3ry.recyclerviewpractice.DangerItem;
import com.myst3ry.recyclerviewpractice.pojo.RedDanger;

public final class RedDangerHolderBinder extends DangerHolderBinder {

    private final RedDanger mRedDangerItem;

    public RedDangerHolderBinder(final DangerItem dangerItem, final int viewType) {
        super(viewType);
        this.mRedDangerItem = (RedDanger) dangerItem;
    }

    @Override
    public void bindViewHolder(RecyclerView.ViewHolder holder) {
        final DangerAdapter.RedDangerViewHolder redHolder = (DangerAdapter.RedDangerViewHolder) holder;
        redHolder.mRedLevelDesc.setText(mRedDangerItem.getDescription());
    }

    @Override
    public DangerItem getItem() {
        return mRedDangerItem;
    }
}
