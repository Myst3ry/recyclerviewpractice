package com.myst3ry.recyclerviewpractice.binder;

import android.support.v7.widget.RecyclerView;

import com.myst3ry.recyclerviewpractice.DangerAdapter;
import com.myst3ry.recyclerviewpractice.DangerItem;
import com.myst3ry.recyclerviewpractice.pojo.YellowDanger;

public final class YellowDangerHolderBinder extends DangerHolderBinder {

    private final YellowDanger mYellowDangerItem;

    public YellowDangerHolderBinder(final DangerItem dangerItem, final int viewType) {
        super(viewType);
        this.mYellowDangerItem = (YellowDanger) dangerItem;
    }

    @Override
    public void bindViewHolder(RecyclerView.ViewHolder holder) {
        final DangerAdapter.YellowDangerViewHolder yellowHolder = (DangerAdapter.YellowDangerViewHolder) holder;
        yellowHolder.mYellowLevelDesc.setText(mYellowDangerItem.getDescription());
    }

    @Override
    public DangerItem getItem() {
        return mYellowDangerItem;
    }
}
