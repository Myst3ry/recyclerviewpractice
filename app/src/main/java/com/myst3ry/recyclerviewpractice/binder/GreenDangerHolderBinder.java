package com.myst3ry.recyclerviewpractice.binder;

import android.support.v7.widget.RecyclerView;

import com.myst3ry.recyclerviewpractice.DangerAdapter;
import com.myst3ry.recyclerviewpractice.DangerItem;
import com.myst3ry.recyclerviewpractice.pojo.GreenDanger;

public final class GreenDangerHolderBinder extends DangerHolderBinder {

    private final GreenDanger mGreenDangerItem;

    public GreenDangerHolderBinder(final DangerItem dangerItem, final int viewType) {
        super(viewType);
        this.mGreenDangerItem = (GreenDanger) dangerItem;
    }

    @Override
    public void bindViewHolder(RecyclerView.ViewHolder holder) {
        final DangerAdapter.GreenDangerViewHolder greenHolder = (DangerAdapter.GreenDangerViewHolder) holder;
        greenHolder.mGreenLevelDesc.setText(mGreenDangerItem.getDescription());
    }

    @Override
    public DangerItem getItem() {
        return mGreenDangerItem;
    }
}
