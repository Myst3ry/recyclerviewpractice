package com.myst3ry.recyclerviewpractice.binder;

import android.support.v7.widget.RecyclerView;

import com.myst3ry.recyclerviewpractice.DangerItem;

public abstract class DangerHolderBinder {

    protected final int mViewType;

    DangerHolderBinder(final int viewType) {
        this.mViewType = viewType;
    }

    public abstract void bindViewHolder(final RecyclerView.ViewHolder holder);

    public abstract DangerItem getItem();

    public int getViewType() {
        return mViewType;
    }
}
