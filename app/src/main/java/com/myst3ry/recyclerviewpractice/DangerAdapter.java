package com.myst3ry.recyclerviewpractice;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myst3ry.recyclerviewpractice.binder.DangerHolderBinder;
import com.myst3ry.recyclerviewpractice.binder.GreenDangerHolderBinder;
import com.myst3ry.recyclerviewpractice.binder.RedDangerHolderBinder;
import com.myst3ry.recyclerviewpractice.binder.YellowDangerHolderBinder;
import com.myst3ry.recyclerviewpractice.factory.DangerHolderFactory;
import com.myst3ry.recyclerviewpractice.factory.GreenDangerHolderFactory;
import com.myst3ry.recyclerviewpractice.factory.RedDangerHolderFactory;
import com.myst3ry.recyclerviewpractice.factory.YellowDangerHolderFactory;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class DangerAdapter extends RecyclerView.Adapter {

    private final List<DangerHolderBinder> mDangerBinders;
    private SparseArray<DangerHolderFactory> mDangerFactories;

    public DangerAdapter() {
        mDangerBinders = new ArrayList<>();
        initFactories();
    }

    private void initFactories() {
        mDangerFactories = new SparseArray<>();
        mDangerFactories.put(DangerType.GREEN.mType, new GreenDangerHolderFactory());
        mDangerFactories.put(DangerType.YELLOW.mType, new YellowDangerHolderFactory());
        mDangerFactories.put(DangerType.RED.mType, new RedDangerHolderFactory());
    }

    private DangerHolderBinder generateBinder(final DangerItem item) {
        switch (item.getType()) {
            case GREEN:
                return new GreenDangerHolderBinder(item, DangerType.GREEN.mType);
            case YELLOW:
                return new YellowDangerHolderBinder(item, DangerType.YELLOW.mType);
            case RED:
                return new RedDangerHolderBinder(item, DangerType.RED.mType);
            default:
                return null;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final DangerHolderFactory factory = mDangerFactories.get(viewType);
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return factory.createDangerHolder(parent, inflater);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final DangerHolderBinder binder = mDangerBinders.get(position);
        if (binder != null) {
            binder.bindViewHolder(holder);
        }
    }

    @Override
    public int getItemCount() {
        return mDangerBinders.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mDangerBinders.get(position).getViewType();
    }

    public void setInitialDangers(@NonNull final List<DangerItem> initialDangersList) {
        mDangerBinders.clear();
        for (DangerItem item : initialDangersList) {
            mDangerBinders.add(generateBinder(item));
        }
        notifyDataSetChanged();
    }

    public void addDangerItem(@NonNull final DangerItem item) {
        mDangerBinders.add(0, generateBinder(item));
        notifyItemInserted(0);
    }

    /*Holders*/

    public static final class GreenDangerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.green_lvl_desc)
        public TextView mGreenLevelDesc;

        public GreenDangerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static final class YellowDangerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.yellow_lvl_desc)
        public TextView mYellowLevelDesc;

        public YellowDangerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static final class RedDangerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.red_lvl_desc)
        public TextView mRedLevelDesc;

        public RedDangerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
