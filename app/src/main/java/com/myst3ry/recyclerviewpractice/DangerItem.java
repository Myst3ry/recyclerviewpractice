package com.myst3ry.recyclerviewpractice;

import android.os.Parcelable;

public interface DangerItem extends Parcelable {

    DangerType getType();

    String getDescription();

    void setDescription(final String description);
}
