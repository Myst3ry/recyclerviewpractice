package com.myst3ry.recyclerviewpractice.pojo;

import android.os.Parcel;

import com.myst3ry.recyclerviewpractice.DangerItem;
import com.myst3ry.recyclerviewpractice.DangerType;

public final class YellowDanger implements DangerItem {

    public static final Creator<YellowDanger> CREATOR = new Creator<YellowDanger>() {

        @Override
        public YellowDanger createFromParcel(Parcel source) {
            return new YellowDanger(source);
        }

        @Override
        public YellowDanger[] newArray(int size) {
            return new YellowDanger[size];
        }
    };

    private final DangerType mType = DangerType.YELLOW;
    private String mDescription;

    public YellowDanger(final String description) {
        this.mDescription = description;
    }

    protected YellowDanger(Parcel in) {
        mDescription = in.readString();
    }

    @Override
    public DangerType getType() {
        return mType;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(final String description) {
        this.mDescription = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDescription);
    }
}
