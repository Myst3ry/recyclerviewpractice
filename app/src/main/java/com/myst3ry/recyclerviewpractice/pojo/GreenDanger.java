package com.myst3ry.recyclerviewpractice.pojo;

import android.os.Parcel;

import com.myst3ry.recyclerviewpractice.DangerItem;
import com.myst3ry.recyclerviewpractice.DangerType;

public final class GreenDanger implements DangerItem {

    public static final Creator<GreenDanger> CREATOR = new Creator<GreenDanger>() {

        @Override
        public GreenDanger createFromParcel(Parcel source) {
            return new GreenDanger(source);
        }

        @Override
        public GreenDanger[] newArray(int size) {
            return new GreenDanger[size];
        }
    };

    private final DangerType mType = DangerType.GREEN;
    private String mDescription;

    public GreenDanger(final String description) {
        this.mDescription = description;
    }

    protected GreenDanger(Parcel in) {
        mDescription = in.readString();
    }

    @Override
    public DangerType getType() {
        return mType;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(final String description) {
        this.mDescription = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDescription);
    }
}

