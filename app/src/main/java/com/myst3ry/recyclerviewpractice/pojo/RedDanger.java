package com.myst3ry.recyclerviewpractice.pojo;

import android.os.Parcel;

import com.myst3ry.recyclerviewpractice.DangerItem;
import com.myst3ry.recyclerviewpractice.DangerType;

public final class RedDanger implements DangerItem {

    public static final Creator<RedDanger> CREATOR = new Creator<RedDanger>() {

        @Override
        public RedDanger createFromParcel(Parcel source) {
            return new RedDanger(source);
        }

        @Override
        public RedDanger[] newArray(int size) {
            return new RedDanger[size];
        }
    };

    private final DangerType mType = DangerType.RED;
    private String mDescription;

    public RedDanger(final String description) {
        this.mDescription = description;
    }

    protected RedDanger(Parcel in) {
        mDescription = in.readString();
    }

    @Override
    public DangerType getType() {
        return mType;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(final String description) {
        this.mDescription = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDescription);
    }
}

